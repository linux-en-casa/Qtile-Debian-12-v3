# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

########################################################
#  Librerías - módulos - funciones y comandos a importar

from libqtile import bar, layout, qtile, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

#  Adicionales
import os
import subprocess
from libqtile import hook

#########################################################
#  Variables

mod = "mod4"

terminal = guess_terminal()
# terminal = "alacritty"
# terminal = "terminator"

# lanzar rofi
rofi = "rofi -show-icons -show drun"

# capturar la pantalla - screenshooter
capturar = "xfce4-screenshooter"

# wlogout para salir - sirve en wayland
wlogout = "wlogout"
# debo revisarlo pues no funciona para cerrar sesión

##########################################################
#  Atajos de teclado


keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),

    # agrego los mismos atajos con flechas - tomado de Arco Linux - Qtile
    Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "Right", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "Down", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "Up", lazy.layout.up(), desc="Move focus up"),



    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # agrego los mismos atajos con flechas - tomado de Arco Linux - Qtile
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc="Move window up"),



    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),

    # Nuevamente estos 4 atajos con flechas - gracias a Arco Linux
    Key([mod, "control"], "Left", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "Right", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "Down", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "Up", lazy.layout.grow_up(), desc="Grow window up"),


    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    ###########
    #  También agrego estos otros
    #  Ver las Variables -  arriba

    # lanzar rofi con Super + alt + d
    Key([mod, "mod1"], "d", lazy.spawn(rofi), desc="Lanzar Rofi"),

    # Capturar la pantalla - screenshooter
    Key([], "Print", lazy.spawn(capturar), desc="Capturar Pantalla"),

    # lanzar wlogout con Super + shift + x
    Key([mod, "shift"], "x", lazy.spawn(wlogout), desc="Lanzar wlogout"),



    # teclas de volumen
    # se debe instalar alsa-utils para que sirva el comando amixer
    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle"), desc="Silencio - Mute"),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -c 0 sset Master 5- unmute"), desc="Bajar Volumen - Volume down"),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -c 0 sset Master 5+ unmute"), desc="Subir Volumen - Volume up"),

    ########################
    # Hasta aquí


    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    # deshabilito esto pues no funciona con xfce
    # Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),


#   Agrego los atajos de teclado de modo flotante y full screen
#   Creo que este archivo lo tomé de alpine linux - luego paso a ubuntu
#   y tal vez por eso no estan los modos flotante y full screen
#   que si están en el archivo de configuración por defecto cuando compilé qtile
#   aqui en debian

    Key(
        [mod],
        "f",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen on the focused window",
    ),
    Key([mod], "t", lazy.window.toggle_floating(), desc="Toggle floating on the focused window"),



]

#################################
# fin de los atajos de teclado


# los grupos a, b y c son las fakescreens pequeñas que estan en la pantalla hdmi
# los voy a declarar en el mismo orden en que declaro los fake screens más abajo

groups = [Group(i) for i in "abc0123456789"]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # Cambio estas opciones de abajo
            # mod1 + shift + letter of group = switch to & move focused window to group
            # Key(
            #     [mod, "shift"],
            #     i.name,
            #     lazy.window.togroup(i.name, switch_group=True),
            #     desc="Switch to & move focused window to group {}".format(i.name),
            # ),
            # Or, use below if you prefer not to switch to that group.
            # mod1 + shift + letter of group = move focused window to group
            Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
                 desc="move focused window to group {}".format(i.name)),
        ]
    )

layouts = [
    layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font="sans bold",
    fontsize=15,
    padding=1,
)
extension_defaults = widget_defaults.copy()


##########################################################################################
##########              CONFIGURACIÓN DE LAS PANTALLAS              ######################
##########################################################################################
#
# Febrero 12  de 2024
# Tengo 3 pantallas, organizadas con arandr así:
# 1 - la pantalla de 32 pulgadas que me regaló Manuel Cabrera - DriveMeca
# Tiene una resolución de 2560x1440 - Se conecta por hdmi
# 2 - la pantalla Samsung VGA de 1440x900
# 3 - La pantalla interna de 1366x768
# aunque físicamente están 3 - 1 - 2
# tal vez las cambio también fisicamente
# dejo de primera la pantalla de 32 pulgadas para ver si los juegos de steam se abren 
# por defecto en esa pantalla

# Divido la pantalla hdmi en 4 fake screens
# que me sirven para resolver el problema de la franja oscura del lado derecho
# y de paso puedo organizar las escenas en OBS


#############################################################################################

# la pantalla hdmi de 2560 x 1440 la divido en 4 fake screens

# +------------------------------------------------------------------------+
# |                                                    |                   |
# |                                                    |                   |
# |                                                    |     screen a      |
# |                                                    |                   |
# |                                                    |     web cam       |
# |                 Pantalla Principal                 |                   |
# |                    1920 x 1080                     |                   |
# |                                                    |-------------------|
# |                      screen 1                      |                   |
# |                                                    |                   |
# |                                                    |                   |
# |                                                    |                   |
# |                                                    |     screen b      |
# |                                                    |                   |
# |                                                    |       chat        |
# |----------------------------------------------------|     imágenes      |
# |                                                    |      juegos       |
# |         Espacio Libre para screenkey               |                   |
# |                                                    |     Pruebas       |
# |   |--------------------------------------------|   |    con python     |
# |   |                                            |   |                   |
# |   |                 screen c - cava            |   |                   |
# |   |                                            |   |                   |
# +------------------------------------------------------------------------+

############################################################################
############################################################################

# aquí empieza la configuración de las pantallas - lo de arriba son solo comentarios

fake_screens = [


###########################################################################
#  primero las screens a, b y c
#  pues están en ese orden cuando declaré los grupos más arriba

########
#  fake screen a que sería un recuadro arriba - derecha de la pantalla hdmi para poner el logo o la webcam
#  tendría un ancho de 600 y alto de 480 -  empezando en la coordenada  3306,20
########
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayout(),
                widget.GroupBox(),
                widget.WindowName(),
            ],
            30,
        ),
        x=3306,
        y=20,
        width=600,
        height=480

    ),

# fin de la fake screen a


########
#  sigue la fake screen b que sería una barra vertical al lado derecho de la pantalla hdmi
#  tendría un ancho de 600 y alto de 900 -  empezando en la coordenada  3306,520
#  el ancho es de 600 para que firefox quede en formato movil y me sirva para los directos
#  y videos verticales
###

    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentLayout(),
                widget.GroupBox(),
                widget.WindowName(),
            ],
            30,
        ),
        x=3306,
        y=520,
        width=600,
        height=900

    ),

# fin de la fake screen b

########
#  sigue la config de la fake screen c - en la parte de abajo de la screen 1
#  con solo 230 de altura y ancho de 1700
#  empezamos en la coordenada - 1476.1200
#  aquí podría poner las barras de cava
#  notar que entre la screen 1 y la screen c hay un espacio vacio
#  donde pongo el screenkey para los videos de obs

    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentLayout(),
                widget.GroupBox(),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                widget.Clock(format="%Y-%m-%d %a %I:%M %p"),
                widget.Volume(),
                widget.Battery(),
            ],
            30,
        ),
        x=1476,
        y=1200,
        width=1700,
        height=230

    ),

#  fin de la configuración de la screen c
#  y fin de la partición de la pantalla hdmi



############################################################################################


#  sigue pantalla interna del portátil que sería la screen 0
#  resolución de  1366 x 768 y empieza en 0.0
#  usaría los espacio de trabajo 0 y 9

    Screen(
        bottom=bar.Bar(
            [
                widget.TextBox("Debian GNU/Linux 12 Bookworm", name="Debian"),
                widget.CurrentLayout(),
                widget.GroupBox(),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                # widget.TextBox("default config", name="default"),
                widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                # widget.Systray(),
                widget.Clock(format="%Y-%m-%d %a %I:%M %p"),
                widget.Volume(),
                widget.Battery(),
            ],
            30,
        ),
        x=0,
        y=0,
        width=1366,
        height=768

    ),


# fin screen 0 de la pantalla interna


################################################################

# esta sería la fake screen 1 de 1920 x 1080
# empezando en 1376,10 solo para dejar un marco o borde para escenas de obs
# x en 1376  y = 10
# ancho  1920 y altura 1080
# en esta screen usaría los números impares 1, 3, 5 y 7

    Screen(
        top=bar.Bar(
            [
                widget.TextBox("Debian GNU/Linux 12 Bookworm", name="Debian"),
                widget.CurrentLayout(),
                widget.GroupBox(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),

                widget.Sep(padding = 20),
                widget.Volume(),
                widget.Systray(),
                widget.Clock(format="%Y-%m-%d %a %I:%M %p"),
                widget.QuickExit(),
            ],
            36,
        ),
        x=1376,
        y=10,
        width=1920,
        height=1080
        # wallpaper = "/home/linuxencasa/.config/qtile/wallpapers/wp1.png",
        # wallpaper_mode = "fill"
        # wallpaper lo hago con feh
        # tal vez estas lineas me sirvan si pruebo qtile con wayland

    ),


#  fin configuracion de la fake screen 1


############################################################################################

#  sigue la configuracion de la pantallas vga

#########
# pantalla samsung conectada por puerto VGA
# screen 2 que sería la pantalla externa DP-1
# tiene una resolución de 1440x900
# empezando en la coordenada 3926,0
# en esta screen usaría los números pares 2, 4, 6 y 8


    Screen(
        bottom=bar.Bar(
            [
                widget.TextBox("Debian GNU/Linux 12 Bookworm", name="Debian"),
                widget.CurrentLayout(),
                widget.GroupBox(),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                widget.Clock(format="%Y-%m-%d %a %I:%M %p"),
                widget.Volume(),
                widget.Battery(),
            ],
            36,
        ),
        x=3926,
        y=0,
        width=1440,
        height=900

    ),

#  fin de la screen 2



#############################################################################################
# si necesito aquí puedo agregar otra pantalla


#  ojo - ojo - atención
#  este parentesis cierra la configuración de las pantallas

]


##########################################################################################
##########################################################################################
#               fin de la configuración de las pantallas
##########################################################################################
##########################################################################################


# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
# wmname = "LG3D"

wmname = "Qtile"



###########################################################################
#  Adicionales
###########################################################################
#  HOOK STARTUP - Ver comienzo del archivo
@hook.subscribe.startup_once
def autostart():
    if qtile.core.name == "x11":
        autostartscript = "~/.config/qtile/autostart_x11.sh"
    elif qtile.core.name == "wayland":
        autostartscript = "~/.config/qtile/autostart_wayland.sh"

    home = os.path.expanduser(autostartscript)
    subprocess.Popen([home])

###########################################################################
