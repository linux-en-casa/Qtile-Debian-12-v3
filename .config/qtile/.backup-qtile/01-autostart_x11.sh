#!/bin/bash

# en alpine no está nitrogen - intento con feh
# nitrogen --restore &

# creo que necesito ejecutar el script de arandr para configurar las pantallas
~/.screenlayout/resolucion-3p.sh &



# copio los comandos de bspwm
# feh para el fondo de la pantalla
feh --bg-center "${HOME}/.config/qtile/wallpapers/wp1.png" "${HOME}/.config/qtile/wallpapers/wp2.png"  "${HOME}/.config/qtile/wallpapers/wp3.png"  &

# picom &


#  iniciar pipewire
# /usr/libexec/pipewire-launcher &

#  teclado en espaniol
# setxkbmap -model pc105 -layout es &

#  iniciar D-Bus session
#  se necesita para que arranquen las apps con flatpak
#  dbus-run-session -- sh &
#  aunque no ha funcionado - seguiré revisando


