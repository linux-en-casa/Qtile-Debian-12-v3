#!/bin/bash

#########################################################################
# Primero el script de arandr para configurar las pantallas
~/.screenlayout/resolucion-3p.sh &
# Pantalla interna - luego pantalla de 32 y al final pantalla vga
# para que coincida con la configuración de las fake screens
# en el archivo de configuración de qtile.
# Si necesito un cambio - solo creo otro archivo con arandr

#########################################################################
# fondo de pantalla
# feh - antes usaba nitrogen

# feh --bg-center "${HOME}/.config/qtile/wallpapers/wp1.png" "${HOME}/.config/qtile/wallpapers/wp2.png"  "${HOME}/.config/qtile/wallpapers/wp3.png"  &

# las imagenes dentro de directorio wallpapers
# una para cada Pantalla - debo ajustar manualmente la resolución de las imagenes
# según la resolución de las pantallas
# 1366x768, 2560x1440, 1440x900

# Abril de 2024 - ahora utilizo variety que viene en los repos de debian
# sudo apt install variety
variety &


#########################################################################
# el polkit - para abrir apps que necesitan sudo - como gparted o synaptic
# uso policykit-1-gnome, que viene o venía instalado por defecto en debian
# en esta ocasión tuve que instalarlo manualmente
# y luego debo lanzar la app:
/usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1 @

##########################################################################



##########################################################################




# todo lo demás son solo pruebas o comentarios

##########################################################################
# picom para las transparencias
# picom &

#  lo que sigue son pruebas que hice en alpine linux
#  cuanto intentaba arrancar qtile solo
#  al final usé xfce con qtile

# en alpine no está nitrogen - intento con feh
# nitrogen --restore &


#  iniciar pipewire
# /usr/libexec/pipewire-launcher &

#  teclado en espaniol
# setxkbmap -model pc105 -layout es &

#  iniciar D-Bus session
#  se necesita para que arranquen las apps con flatpak
#  dbus-run-session -- sh &
#  aunque no ha funcionado - seguiré revisando


