# Qtile en Debian GNU/Linux 12 Bookworm Versión 3

Archivos de configuración de Qtile en el Portátil H.P. ProBook 430 G3 configurado en Abril de 2024.

## Bitácora
La instalación de Qtile la hice siguiendo el video de Linux Dabbler:
[Linux Dabbler - Qtile](https://youtu.be/IoCwRZkm9l8?si=0iVh6NkBlTkla0-1) .

Aunque en esta ocasión estoy empezando a partir del Escritorio GNOME.

Hice varios videos y directos explicando la configuración paso a paso.

Video de la Instalación de Qtile:
[Qtile - Instalación Debian 12](https://youtu.be/4orUSvlesVM) .

Segundo Video explicando algunos atajos de teclado:
[Qtile - Atajos de teclado](https://youtu.be/wa7KxR5e54s) .

También subí un video con el resumen de la instalación de Qtile:
[Qtile - Instalación en 17 minutos](https://youtu.be/f4lOxANgJ5s) .

Luego expliqué la configuración del archivo de qtile durante 3 dias:

Día 1:
[Qtile Config - día 1](https://youtube.com/live/fBzDMNPNQfc) .

Día 2:
[Qtile Config - día 2](https://youtube.com/live/1eALsoKANvY) .

Día 3:
[Qtile Config - día 3](https://youtube.com/live/Utq_w5fCIDI) .

Nota:  Creo que el día 3 hice un directo o un video adicional,
pero ahora no lo encuentro. O tal vez fue este directo:
[Qtile Config - día 3 - Adicional](https://youtube.com/live/wrwqWhVdb4I) .



## Configuración
Casi todo se configura con 2 archivos:

El archivo de qtile ~/.config/qtile/config.py

Y el script ~/.config/qtile/autostart_x11.sh

Dejo varias versiones de estos archivos, incluyendo los archivos de configuración de los 3 días.

La configuración final es con 3 pantallas:
* Pantalla AOC de 1440x900 conectada por puerto hdmi - es la fake screen 1
* Pantalla Samsung 1440x900 conectada por puerto vga - la divido en los fake screens 2 y a - 
este último es para el screenkey de los directos.
* Pantalla interna del portatil 1360x768 - es la fake screen 3.

La configuración de las pantallas se hizo con arandr y se generó el script que está en:
~/.screenlayout/resolucion-2p.sh


Tengo un archivo autostart_x11.sh con el que se configura la resolución de la pantalla, el fondo de pantalla; 
antes con feh, ahora con Variety.

Con el archivo autostart también se configura el policykit - 
polkit para abrir apps como synaptic que necesitan password.

## Aplicaciones que utilizo
Adicional a qtile se utiliza:
* rofi y dmenu para lanzar aplicaciones
* arandr para la resolución de la pantalla
* policykit-i-gnome para el polkit
* feh y/o variety para el fondo de pantalla
* wlogout para cerrar sesión, reiniciar y apagar
* terminator como emulador de terminal
* audio con pipewire, aunque ya lo configura automáticamente GNOME

## Notas
* Esta configuración es en X11, en Wayland la he probado usando Debian 13 - Trixie, pero no la he podido configurar bien y ni siquiera guardé los archivos :-(
* Para el tema de rofi se debe dar comando rofi-theme-selector.
* Para cerrar sesión y apagar se usa wlogout que funciona tanto en waylando como en xorg.
* Agrego varios atajos de teclado tomados de la configuración de Qtile de ArcoLinux.
* Los archivos de configuración de los días 1 y 2 no utiliza fake screens.  Así que podría servir para configurar una o dos pantallas nada más.

## Links adicionales:
Más videos y Directos con la configuración en YT:
[YouTube, Linux en Casa](https://www.youtube.com/LinuxenCasa) .
 
También en Twitch:
[Twitch, Linux en Casa](https://www.twitch.tv/linuxencasa) .


**GNU General Public License v3.0**

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


